import React from 'react';
import BlogList1 from '../assets/img/blog-list-1.png';
import BlogList2 from '../assets/img/blog-list-2.png';
import Author from '../assets/img/author.png';
import Linkio from '../assets/img/linkio.png';
import Feedspot from '../assets/img/feedspot.png';

const FaqSection = (props) => {
  return (
    <section class="blog-page-sec">
      <div class="container">
        <div class="blog-page-in">
          <div class="blog-list-top">
            <div class="row align-items-center">
              <div class="col-lg-6">
                <div class="blog-list-top-img">
                  <a href="single-blog.html">
                    <img src={BlogList1} />
                  </a>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="blog-list-box">
                  <h3>
                    <a href="single-blog.html">
                      {props.activePost ? props.activePost.title : '-'}
                    </a>
                  </h3>
                  <p class="blog-desc">
                    {props.activePost ? props.activePost.body : '-'}
                  </p>
                  <a href="single-blog.html" class="btn btn-white arrow-btn">
                    Read more
                  </a>
                  <div class="blog-list-author">
                    <div class="blog-author-img">
                      <img src={Author} />
                    </div>
                    <div class="blog-author-name">
                      <h4>
                        <span>Author : </span>
                        <a href="#">Sam Singh</a>
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="blog-list-bottom">
            <div class="row">
              <div class="col-lg-8">
                <div class="blog-list-row">
                  <div class="row">
                    {props.dataList && props.dataList.length > 0 ? (
                      props.dataList.map((obj) => (
                        <div class="col-md-6">
                          <div class="blog-list-box">
                            <div class="blog-list-img">
                              <a href="single-blog.html">
                                <img src={BlogList2} />
                              </a>
                            </div>
                            <h3>
                              <a href="single-blog.html">
                                Consider These 7 Things Before Moving Into Your
                                New Home
                              </a>
                            </h3>
                            <p class="blog-desc">
                              Leaving your home behind to move into a new one is
                              nerve-wracking as it is with surprises waiting for
                              you ...
                            </p>
                            <a
                              href="single-blog.html"
                              class="btn btn-white arrow-btn"
                            >
                              Read more
                            </a>
                            <div class="blog-list-author">
                              <div class="blog-author-img">
                                <img src={Author} />
                              </div>
                              <div class="blog-author-name">
                                <h4>
                                  <span>Author : </span>
                                  <a href="#">Sam Singh</a>
                                </h4>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    ) : (
                      <p>No data found</p>
                    )}
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="blog-sidebar">
                  <div class="blog-sidebar-box">
                    <h4>As featured on Linkio Top SEO Agencies</h4>
                    <img src={Linkio} />
                  </div>
                  <div class="blog-sidebar-box">
                    <h4>As featured on Feedspot Top 100 UK Marketing Blogs</h4>
                    <img src={Feedspot} />
                  </div>
                  <div class="blog-sidebar-list">
                    <h3>Recent Blog</h3>
                    <ul>
                      <li>
                        <a href="#">
                          Top Software Development Companies in London – Compare
                          Quotes
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          What is Web Application Development – A Beginner’s
                          Guide
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          The Ultimate Guide to Outsource Web Development
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          Web Developer vs Software Developer – What is the
                          Difference?
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          The Ultimate Guide to Micro Influencers in the UK 2020
                        </a>{' '}
                      </li>
                    </ul>
                  </div>
                  <div class="blog-sidebar-list">
                    <h3>Main Services</h3>
                    <ul>
                      <li>
                        <a href="web-design.html">Web Design</a>
                      </li>
                      <li>
                        <a href="web-development.html">Web Development</a>
                      </li>
                      <li>
                        <a href="digital-marketing.html">Digital Marketing</a>
                      </li>
                      <li>
                        <a href="seo-marketing.html">SEO</a>
                      </li>
                      <li>
                        <a href="influencer-marketing.html">
                          Influencer Marketing
                        </a>{' '}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            {props.totalPaginationCount && (
              <div class="blog-pagination">
                <ul>
                  <li class="active">
                    <a href="#">1</a>
                  </li>
                  <li>
                    <a href="#">2</a>
                  </li>
                  <li>
                    <a href="#">3</a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-angle-right"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-angle-double-right"></i>
                    </a>
                  </li>
                </ul>
              </div>
            )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default FaqSection;
