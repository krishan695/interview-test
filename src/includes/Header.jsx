import React from 'react';
import Logo from '../assets/img/blog-logo.png';

const Header = () => {
  return (
    <header className="header">
      <div className="main-header">
        <div className="container">
          <nav className="navbar navbar-expand-lg">
            <a className="navbar-brand" href="blog.html">
              <img src={Logo} />
            </a>

            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#collapsibleNavbar"
            >
              <span className="fa fa-bars"></span>
            </button>

            <div className="collapse navbar-collapse" id="collapsibleNavbar">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link" href="blogging-tools.html">
                    Blogging Tools
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    Google
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="internet-marketing.html">
                    Internet Marketing
                  </a>
                </li>
                <li className="nav-item dropdown">
                  <a className="nav-link" href="services.html">
                    Our Services
                  </a>
                  <div className="sub-menu-toggle"></div>
                  <ul className="sub-menu">
                    <li>
                      <a href="web-design.html">Web Design</a>
                    </li>
                    <li>
                      <a href="web-development.html">Web Development</a>
                    </li>
                    <li>
                      <a href="digital-marketing.html">Digital Marketing</a>
                    </li>
                    <li>
                      <a href="graphic-design.html">Graphic Design</a>
                    </li>
                    <li>
                      <a href="it-services.html">IT Services</a>
                    </li>
                    <li>
                      <a href="mobile-apps.html">Mobile Apps</a>
                    </li>
                    <li>
                      <a href="white-label-solutions.html">
                        White Label Solutions
                      </a>
                    </li>
                    <li>
                      <a href="outsourcing.html">Outsourcing</a>
                    </li>
                    <li>
                      <a href="printing.html">Printing</a>
                    </li>
                    <li>
                      <a href="consultation.html">Consultation</a>
                    </li>
                  </ul>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="advertise.html">
                    Advertise With Us
                  </a>
                </li>
              </ul>
              <div className="header-btn">
                <ul>
                  <li>
                    <a href="#" className="btn request-btn">
                      Request a Free quote
                    </a>
                  </li>
                  <li>
                    <a href="contact-us.html" className="btn">
                      Contact Us
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </header>
  );
};

export default Header;
