import React from 'react';

const ContactSection = () => {
  return (
    <section class="service-contact-sec">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="service-contact-left">
              <h3>Get in touch/ also schedule a call/meet </h3>
              <h4>
                <span>Schedule a call or meeting</span> with our digital
                marketing expert for <span>FREE consultation</span>.
              </h4>
              <a href="#" class="btn arrow-btn red-btn">
                Schedule Consultation
              </a>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="service-contact-form">
              <h4>
                <span>
                  If you want us to prepare a digital marketing scope for your
                  business,
                </span>{' '}
                please fill in the necessary information required for us to
                proceed.
              </h4>
              <form action="" method="">
                <div class="service-form-box">
                  <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" name="" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="tel" name="" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Service Category</label>
                    <select class="form-control">
                      <option>Select</option>
                      <option>Web Design</option>
                      <option>Web Development</option>
                      <option>Digital Marketing</option>
                      <option>Graphic Design</option>
                      <option>IT Services</option>
                      <option>Mobile Apps</option>
                      <option>White Label Solutions</option>
                      <option>Outsourcing</option>
                      <option>Printing</option>
                      <option>Consultation</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Date</label>
                    <input type="date" name="" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Time</label>
                    <select class="form-control">
                      <option>Morning</option>
                      <option>Noon</option>
                      <option>Evening</option>
                    </select>
                  </div>
                </div>
                <div class="submit-btn text-right">
                  <button type="submit" class="btn arrow-btn red-btn">
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContactSection;
