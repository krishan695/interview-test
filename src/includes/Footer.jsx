import React from 'react';
import Drupal from '../assets/img/icons/drupal.png';
import Wordpress from '../assets/img/icons/wordpress.png';
import Joomla from '../assets/img/icons/joomla.png';
import Java from '../assets/img/icons/java.png';
import Angulari from '../assets/img/icons/angular-i.png';
import Html from '../assets/img/icons/html.png';
import Css from '../assets/img/icons/css.png';
import Bootstrap from '../assets/img/icons/bootstrap.png';
import Node from '../assets/img/icons/node.png';

const Footer = () => {
  return (
    <footer class="footer">
      <div class="top-footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-6">
              <div class="row">
                <div class="col-lg-3">
                  <div class="footer-box">
                    <ul class="footer-menu">
                      <li>
                        <a href="index.html">Home</a>
                      </li>
                      <li>
                        <a href="about-us.html">About Us</a>
                      </li>
                      <li>
                        <a href="our-work.html">Our Work</a>
                      </li>
                      <li>
                        <a href="faq.html">Faq's</a>
                      </li>
                      <li>
                        <a href="how-we-work.html">How We Work</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-9">
                  <div class="footer-box">
                    <h3>Services</h3>
                    <ul class="footer-service-menu">
                      <li>
                        <a href="web-design.html">Web Design</a>
                      </li>
                      <li>
                        <a href="web-development.html">Web Development</a>
                      </li>
                      <li>
                        <a href="digital-marketing.html">Digital Marketing</a>
                      </li>
                      <li>
                        <a href="graphic-design.html">Graphic Design</a>
                      </li>
                      <li>
                        <a href="it-services.html">IT Services</a>
                      </li>
                      <li>
                        <a href="mobile-apps.html">Mobile Apps</a>
                      </li>
                      <li>
                        <a href="white-label-solutions.html">
                          White Label Solutions
                        </a>
                      </li>
                      <li>
                        <a href="outsourcing.html">Outsourcing</a>
                      </li>
                      <li>
                        <a href="printing.html">Printing</a>
                      </li>
                      <li>
                        <a href="consultation.html">Consultation</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-6">
              <div class="footer-box footer-contact">
                <h3>Contact Us</h3>
                <ul>
                  <li>
                    <span>Address: </span>Suite 6, 17 Comalco Ct Thomastown,
                    3074
                  </li>
                  <li>
                    <span>Telephone:</span>
                    <a href="tel:03 8595 5246">03 8595 5246</a>
                  </li>
                  <li>
                    <span>Email:</span>
                    <a href="mailto:info@logicsofts.com.au">
                      info@logicsofts.com.au
                    </a>
                  </li>
                </ul>
              </div>
              <div class="footer-box footer-social">
                <h3>Stay With Us</h3>
                <ul>
                  <li>
                    <a href="#">
                      <i class="fa fa-youtube"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-7 col-12 footer-tech-row">
              <div class="footer-box">
                <h3>TECHNOLOGY USED</h3>
                <div class="tech-img">
                  <ul>
                    <li>
                      <img src={Drupal} />
                    </li>
                    <li>
                      <img src={Wordpress} />
                    </li>
                    <li>
                      <img src={Joomla} />
                    </li>
                    <li>
                      <img src={Java} />
                    </li>
                    <li>
                      <img src={Angulari} />
                    </li>
                    <li>
                      <img src={Html} />
                    </li>
                    <li>
                      <img src={Css} />
                    </li>
                    <li>
                      <img src={Bootstrap} />
                    </li>
                    <li>
                      <img src={Node} />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <p>
            Copyright - 2020-2021 All Right Reserved - Logicsofts Australia Ptv
            Ltd. - ABN29611694529
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
