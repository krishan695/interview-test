import React from 'react';

import Header from './includes/Header';
import Newsletter from './includes/Newsletter';
import Footer from './includes/Footer';
import ContactSection from './includes/ContactSection';
import FapSection from './includes/FapSection';
import axios from 'axios';

class App extends React.Component {
  state = {
    showLoader: false,
    dataList: true,
    activePost: {},
    totalPaginationCount: 0,
  };

  fetchBlogList = async () => {
    try {
      this.setState({
        showLoader: true,
      });
      const response = await axios.get(
        'https://jsonplaceholder.typicode.com/posts'
      );
      if (response.data && response.data.length > 0) {
        this.setState({
          activePost: response.data[0],
          dataList: response.data,
          totalPaginationCount: response.data.length / 4,
        });
      } else {
        alert('Please reload page.');
      }
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        showLoader: false,
      });
    }
  };

  componentDidMount() {
    // fetch data;
    this.fetchBlogList();
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Newsletter />
        <FapSection
          dataList={this.state.dataList}
          activePost={this.state.activePost}
          totalPaginationCount={this.state.totalPaginationCount}
        />
        <ContactSection />
        <Footer />
      </div>
    );
  }
}

export default App;
